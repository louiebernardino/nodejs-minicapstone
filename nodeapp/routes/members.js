// members - routes 
//Declare dependencies and model
const Member = require("../models/members");
const express = require("express");
const router = express.Router(); //to handle routing
const bcrypt = require("bcryptjs")
const auth = require("../middleware/auth")
const multer = require("multer")
const sharp = require("sharp")



//UPLOAD IMAGES

//Configure Multer (we can configure where the file can be saved)
const upload = multer({
	// dest: "images/members/profilepictures",
	limits: {
		filesize: 1000000 //max file size in bytes
	},
	fileFilter(req, file, cb) {
		//https://regex101.com
		if(!file.originalname.match(/\.(jpg|jpeg|png|PNG|JPEG)$/)) {
			return cb(new Error("Please upload an image only!"))
		}

		cb(undefined, true)

	}

	// "dest:" - destination
})

//Sample: Endpoint to upload a file
router.post("/upload", upload.single("profpicture"), auth, async (req,res) => {

	const buffer = await sharp(req.file.buffer).resize({
		width: 50,
		height: 50
	})
	.png()
	.toBuffer()

	req.member.profilePic = buffer
	await req.member.save()
	// try {
	// 	res.send({ message: "Successfully uploaded image!" })
	// } catch(e) {
	// 	//BAD REQUEST
	// 	res.status(400).send({ error: e.message })
	// }
	// res.send({ message: "Successfully uploaded image!" })
	res.send(req.member)
}, (error, req, res, next) => {
	res.status(400).send({error: error.message})}) 

//DELETE PROFILEPIC
router.delete("/picdelete", auth, async (req,res) =>{
	try{
	req.member.profilePic = undefined;
	await req.member.save();
	res.send(req.member)
	}catch(e){
		res.status(400).send(e)
	}
})

// DISPLAY AN IMAGE -> Who can view profilePics? Anyone
router.get("/:id/upload", async (req, res) => {
	try {
		const member = await Member.findById(req.params.id)
		if (!member || !member.profilePic) {
			return res.status(404).send("Profile Pic doesn't exist!")
		}

		//send back the correct data and tell the client what type of data it will receive
		res.set("Content-Type", "image/png")
		res.send(member.profilePic)

	} catch(e) {
		res.status(500).send(e)
	}
})

//LOGIN
router.post("/login", async(req, res) => {
	try	{
		//submit email and password
		const member = await Member.findOne({$or: [{email: req.body.email}, {username: req.body.email}]})
		if(!member) {
			return res.send({"message": "Invalid Login Credentials!"})
		}

		const isMatch = await bcrypt.compare(req.body.password, member.password)

		if(!isMatch) {
			return res.send({ "message": "Invalid Login Credentials!"})
		}

		//generate token
		const token = await member.generateAuthToken()
		res.send({member, token})
	} catch(e) {
		res.status(500).send(e)
	}
})

//CREATE A MEMBER
router.post("/", async (req, res) => {
	const member = new Member(req.body)
	try {
		await member.save(); 
		res.status(201).send(member);
	} catch(e) {
		res.status(400).send(e.message);
	}
});

//GET ALL MEMBERS
router.get("/", auth, async (req, res) => {
	const archived = await Member.find({archived: req.query.archived})
	try {
		if(!req.query.archived) {
			const member = await Member.find()
			return res.send(member)
		}
		return res.send(archived)
	} catch(e) {
		return res.status(404).send(e.message)
	}
});

//7) GET LOGIN USER'S PROFILE
router.get("/me", auth, async (req, res) => {
	res.send(req.member)
	// res.send({req.token, req.member})
})

//3) GET ONE MEMBER
router.get("/:id", async (req, res) => {
	const _id = req.params.id;
	try {
		const member = await Member.findById(_id);
		if(!member) {
			return res.status(404).send("Member doesn't exist!");
		}
		res.send(member)
	} catch(e) {
		return res.status(500).send(e);
	}
});

//4) UPDATE ONE (OWN PROFILE)
router.patch("/me", auth, async (req, res) => {
	const updates = Object.keys(req.body);
	// console.log(updates);

	const allowedUpdates = ["firstName", "lastName", "position", "password", "teamId"];

	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))

	if(!isValidUpdate) {
		return res.status(400).send({ error: "Invalid update"});
	}
	//end of function

	try {
		// const member = await Member.findByIdAndUpdate(_id, req.body, { new: true });

		// const member = await Member.findById(_id);
		updates.map(update => (req.member[update] = req.body[update]))

		// if(!member) {
		// 	return res.status(404).send("Member doesn't exist!");
		// }
		await req.member.save();
		res.send(req.member);
	} catch(e) {
		return res.status(500).send(e);
	}
});

//DELETE SELF (SOFT DELETE OF THE ONE LOGGED IN)
router.delete("/deleteself", auth, async (req, res) => {
	// console.log(req.member.deleted)
	try {
		const status = req.member;
		if(status.isDeactivated === true) {
			res.status(404).send("Member does not exist")
		}
		status.archived = true;
		await status.save();
		res.send(status);
	}catch(e) {
		res.status(500).send(e);
	}
})

// //GET ALL ACTIVE MEMBERS
// router.get("/activemembers", auth, async (req, res) => {
// 	try {
// 	} catch(e) {
// 		return res.status(404).send(e.message)
// 	}
// });

//8) LOGOUT


//9.) LOGOUT ALL

router.post('/logoutall', auth, async (req, res) => {
  try {
    console.log('======req.member======');
    console.log(req.member);
    console.log('======req.member.tokens======');
    console.log(req.member.tokens);
    console.log('======req.tokens======');
    console.log(req.token);
    /*
      CLUES:
      Carefully study the results in your console. 
      You are logged out IF req.members.tokens returns an empty array.
      Don't forget to save changes! This part of the code should behave synchronously. ;)
      Send back the message, "You've been successfully logged out of all devices!"
    */
 
  } catch (e) {
    //Send back status 500 and the error
  }
});

module.exports = router;