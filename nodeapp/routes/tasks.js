//Declare dependencies and model
const Task = require("../models/tasks");
const express = require("express");
const router = express.Router(); //to handle routing
const auth = require("../middleware/auth")

//CREATE A TASK
router.post("/", auth, async (req, res) => {
	const task = new Task({
		//use SPREAD OPERATOR to copy the body
		...req.body, 
		memberId: req.member._id
	})
	try {
		await task.save(); 
		res.status(201).send(task)
	} catch(e) {
		res.status(400).send(e)
	}
})

//GET ALL TASKS OF LOGGED IN MEMBER
router.get("/", auth, async (req, res) => {
	// console.log(req.query) check req.query


	const match = {}; //instantiating an object named match
	const sort = {} //instantiating an object named sort
	if(req.query.isCompleted) {
		match.isCompleted = req.query.isCompleted //this line gives value to the object match (the value would be the one on the url)
	}

	//TEST
	// console.log(req.query.sortBy)
	// console.log(req.query.sortBy.split(":"))

	if(req.query.sortBy) {
		const parts = req.query.sortBy.split(":")
		sort[parts[0]] = parts[1] === "desc" ? -1 : 1
	}

	console.log(sort)
	try {
		//approach 1
		// const tasks = await Task.find({memberId: req.member._id})
		//approach 2
		await req.member.populate({path: "tasks", match, options: {
			limit: parseInt(req.query.limit),
			skip: parseInt(req.query.skip),
			sort //sort: sort
		}

		}).execPopulate()
		res.send(req.member.tasks)
	} catch(e) {
		return res.status(404).send(e)
	}
});

//GET A TASK OF LOGGED IN MEMBER
router.get("/:id", auth,  async (req, res) => {
	const _id = req.params.id;	
	try {
		const task = await Task.findById(_id);
		if(!task) {
			return res.status(404).send("Task doesn't exist!");
		}
		res.send(task)
	} catch(e) {
		return res.status(500).send(e);
	}
});

//UPDATE A TASK - OWNER OF THE TASK CAN UPDATE
router.patch("/:id", auth, async (req, res) => {
	const _id = req.params.id;
	try {
		const task = await Task.findByIdAndUpdate(_id, req.body, { new: true });
		if(!task) {
			return res.status(404).send("Task doesn't exist!");
		}
		res.send(task);
	} catch(e) {
		return res.status(500).send(e);
	}
});

//DELETE A TASK - OWNER OF THE TASK CAN UPDATE
router.delete("/:id", auth, async (req, res) => {
	const _id = req.params.id;
	try {
		const task = await Task.findByIdAndDelete(_id);
		if(!task) {
			return res.status(404).send("Task doesn't exist");
		}
		res.send(task);
	} catch(e) {
		return res.status(500).send(e);
	}
});

module.exports = router;